import { Component, OnInit, ViewChild } from '@angular/core';

import { hdiAnimations } from '../../../animations/index';
import { InspeccionesSiniestrosService } from '../inspecciones-siniestros.service';
import { MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import { InspeccionSiniestro } from '../inspeccion-siniestro.model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-inspecciones-siniestros',
    templateUrl: './listado-siniestros.component.html',
    styleUrls: ['./listado-siniestros.component.scss'],
    animations: hdiAnimations
})
export class ListadoSiniestrosComponent implements OnInit {
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    public inspecciones: MatTableDataSource<InspeccionSiniestro>;
    public displayColumns: string[] = ['fecha', 'fecha servidor', 'nombre','patente','anio', 'estado', 'actions'];
    public states: string[] = []
    constructor(
        private _inspeccionesSiniestrosService: InspeccionesSiniestrosService,
        private _router: Router
    ) { }

    ngOnInit(): void {
        this._inspeccionesSiniestrosService.getInspecciones().subscribe(
            data => {
                this.inspecciones = new MatTableDataSource<InspeccionSiniestro>(data);
                this.inspecciones.sort = this.sort;
                this.inspecciones.paginator = this.paginator;
                this.states = ['EN PROCESO', 'EMITIDA']
            },
            error => {
              console.log('XXXXXXXXXX', error)
            }
        )
    }

    verDetalle(id: string): void {
        this._router.navigate(['/pages/inspecciones-siniestros', id]);
    }

    applyFilter(filterValue: string) {
        this.inspecciones.filter = filterValue.trim().toLowerCase();
    }

    handlerSelect(value, id) {
      this._inspeccionesSiniestrosService.updateState(id, value).subscribe(
        data => { console.log("estado ok") },
        error => { console.log(error)}
      )
    }
}

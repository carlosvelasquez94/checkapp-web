import { Component, OnInit } from "@angular/core";

import { hdiAnimations } from '../../animations/index';

@Component({
    selector: 'app-inspecciones-siniestros',
    templateUrl: './inspecciones-siniestros.component.html',
    styleUrls: ['./inspecciones-siniestros.component.scss'],
    animations: hdiAnimations
})
export class InspeccionesSiniestrosComponent implements OnInit {
    constructor(
    ) { 

    }

    ngOnInit(): void {

    }
}
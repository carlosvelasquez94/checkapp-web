import { Component, OnInit, ViewChild } from '@angular/core';
import { componentFactoryName } from '@angular/compiler';
import { hdiAnimations } from 'src/app/animations';
import { ImagenPipe } from 'src/app/shared/pipes/imagen/imagen.pipe';
import { InspeccionSiniestro } from '../inspeccion-siniestro.model';
import { ActivatedRoute, Router } from '@angular/router';
import { InspeccionesSiniestrosComponent } from '../inspecciones-siniestros.component';
import { InspeccionesSiniestrosService } from '../inspecciones-siniestros.service';
import { saveAs } from "file-saver";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import * as JSZip from 'jszip';
import { forkJoin } from "rxjs";
import { getMatFormFieldDuplicatedHintError } from '@angular/material';
@Component({
	selector: 'app-detalle-siniestro',
	templateUrl: './detalle-siniestro.component.html',
	styleUrls: ['./detalle-siniestro.component.scss'],
	animations: hdiAnimations,
	providers: [ ImagenPipe ]
})
export class DetalleSiniestroComponent implements OnInit {
	public inspeccion: InspeccionSiniestro;
  public displayColumnsDanios: string[] = ['autoparte'];
  private  id:string;
  getRequests= [];
  public env = environment;
	constructor(
		private _activateRoute: ActivatedRoute,
		private _router: Router,
    private _inspeccionesSiniestrosService: InspeccionesSiniestrosService,
    private _http: HttpClient,
	) {}

	ngOnInit(): void {
		this._activateRoute.params.subscribe(params => {
      this.id =  params['id'];
			const id = params['id'];
			this._inspeccionesSiniestrosService.getInspeccion(id).subscribe(
				inspeccion => {
          this.inspeccion = inspeccion
				},
				error => {
          console.log(error, 'zzz')
				}
			)
		})
	}
	verDetalle(id: string): void {
		this._router.navigate(['/pages/inspecciones-siniestros', id]);
  }

  dataFormatter(inspeccion) {
    return `
      Nombre=${inspeccion.nombre}
      Email=${inspeccion.email}
      Telefono=${inspeccion.telefono}
      Patente=${inspeccion.patente}
      Año=${inspeccion.anio}
      Fecha=${inspeccion.fecha}
      Daños=${inspeccion.danios}
      Vehículo=${inspeccion.tipoAutomovil}
      Estado=${inspeccion.estado}
    `
  }

  dowloadImages(id: string): void {
    const { Fotos, Nombre, Patente } = this.inspeccion
    const images = this.imagesToDowload(Fotos)
    this.createGetRequets(images);

    forkJoin(...this.getRequests)
     .subscribe((res) => {
      const zip = new JSZip();
      res.forEach((f, i) => {
        zip.file(`${Fotos[i].Imagen}`, f);
      });
      zip.file('detalle.txt', this.dataFormatter(this.inspeccion))
      /* With file saver */
      zip
        .generateAsync({ type: 'blob' })
        .then(blob => saveAs(blob, `${Patente}-${Nombre}.zip`));

      /* Without file saver */
      // zip
      //   .generateAsync({ type: 'blob' })
      //   .then(blob => {
      //     const a: any = document.createElement('a');
      //     document.body.appendChild(a);

      //     a.style = 'display: none';
      //     const url = window.URL.createObjectURL(blob);
      //     a.href = url;
      //     a.download = `${Patente}-${Nombre}.zip`;
      //     a.click();
      //     window.URL.revokeObjectURL(url);
      //   });
     });
  }

  private imagesToDowload(images: any) {
    //return images.map(image =>  `https://res.cloudinary.com/cvelasquez/image/upload/v1588882475/${image.imagen}.jpg`)
    // return images.map(image =>  `${environment.urlService}/api/image${image.imagen}`)
    return images.map(image => image.imagen)
  }

  private createGetRequets(data: string[]) {
    data.forEach(name => this.getRequests.push(
        this._inspeccionesSiniestrosService.dowloadFile(name)
      ));
  }

  dowloadImage(name) {
    const fullname = name.split('/')
    this._inspeccionesSiniestrosService.dowloadFile(fullname[1]).subscribe(
      (data) => {
        if(data && data != undefined && data != null){
          saveAs(data,name);
        }
      }
    )
  }

}

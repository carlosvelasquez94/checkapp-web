import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import { InspeccionSiniestro } from './inspeccion-siniestro.model';

@Injectable({
	providedIn: 'root'
})
export class InspeccionesSiniestrosService {
	constructor(
		private _http: HttpClient,
	){ }

	getInspecciones() {
		return this._http.get(environment.urlService + '/api/inspeccionSiniestro').pipe(
			map((data:any) => {
				return data.map(inspeccion => {
					return new InspeccionSiniestro(inspeccion);
				});
			})
		);
	}

	getInspeccion(id: string) {
		return this._http.get(environment.urlService + '/api/inspeccionSiniestro/' + id).pipe(
			map((data:any) => {
				return new InspeccionSiniestro(data);
			})
		)
  }

  updateState(id: string, value) {
		return this._http.put(environment.urlService + '/api/inspeccionSiniestro/' + id, { estado: value}).pipe(
			map((data:any) => {
				return new InspeccionSiniestro(data);
			})
		)
  }

  dowloadFile(name) {
    return this._http.post(environment.urlService + '/api/inspeccionSiniestro/downloadImaegeSinister', {name}, {
      responseType : 'blob',
      headers : new HttpHeaders().append('content-type','application/json')
    })
  }
}

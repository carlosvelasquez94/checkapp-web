import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import {
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatSortModule,
} from '@angular/material';
import { MatSelectModule } from '@angular/material/select'

import { InspeccionesSiniestrosRoutingModule } from './inspecciones-siniestros-routing.module'
import { InspeccionesSiniestrosComponent } from './inspecciones-siniestros.component';
import { ListadoSiniestrosComponent } from './listado/listado-siniestros.component';


import { HeaderModule } from '../../components/header/header.module';
import { ImagenPipeModule } from '../../shared/pipes/imagen/imagen.module';
import { DetalleSiniestroComponent } from './detalle/detalle-siniestro.component';



@NgModule({
    declarations: [
        InspeccionesSiniestrosComponent,
        ListadoSiniestrosComponent,
        DetalleSiniestroComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        InspeccionesSiniestrosRoutingModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTabsModule,
        MatSortModule,
        MatCardModule,
        MatInputModule,
        HeaderModule,
        ImagenPipeModule,
        MatSelectModule,
    ]

})
export class InspeccionesSiniestrosModule { }

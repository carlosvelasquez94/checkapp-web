import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InspeccionesSiniestrosComponent } from './inspecciones-siniestros.component';
import { ListadoSiniestrosComponent } from './listado/listado-siniestros.component';
import { DetalleSiniestroComponent } from './detalle/detalle-siniestro.component';

const routes: Routes = [
  {
    path: '',
    component: InspeccionesSiniestrosComponent,
    children: [
      { path: '', component: ListadoSiniestrosComponent },
      { path: ':id', component: DetalleSiniestroComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspeccionesSiniestrosRoutingModule { }

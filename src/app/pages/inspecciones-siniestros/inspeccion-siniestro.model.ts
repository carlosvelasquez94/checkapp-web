import { Foto } from 'src/app/shared/models/foto.model';
import { Danio } from '../danios/danio.model';

export class InspeccionSiniestro {
    private id: string;
    private fecha: Date;
    private fechaServer: Date;
    private hora: string;
    private nombre: string;
    private telefono: string;
    private email: string;
    private patente: string;
    private anio: string;
    private tipoAutomovil: string;
    private estado: string;
    private fotos: Array<Foto>;
    public danios: Array<string>;

    constructor (obj) {
        this.id = obj._id || '';
        this.fecha = obj.fecha || '';
        this.fechaServer = obj.fechaServer || '';
        this.hora = obj.hora || '';
        this.nombre = obj.nombre || '';
        this.telefono = obj.telefono || '';
        this.email = obj.email || '';
        this.patente = obj.patente || '';
        this.anio = obj.anio || '';
        this.estado = obj.estado || '';
        this.tipoAutomovil = obj.tipoAutomovil || '';

        this.fotos = new Array<Foto>();
        if (obj.fotos.length > 0 ) {

            for(let foto of obj.fotos) {
                this.fotos.push(new Foto(foto))
            }
        }
        this.danios = obj.danios || []
    }

    get Id(): string {
        return this.id;
    }

    get Fecha(): Date {
        return this.fecha;
    }

    get FechaServer(): Date {
      return this.fechaServer;
    }
    get Hora(): string {
        return this.hora;
    }

    get Nombre(): string {
        return this.nombre;
    }

    get Telefono(): string {
        return this.telefono;
    }

    get Email(): string {
        return this.email;
    }

    get Patente(): string {
        return this.patente;
    }

    get Anio(): string {
        return this.anio;
    }

    get TipoAutomovil(): string {
        return this.tipoAutomovil;
    }

    get Estado(): string {
        return this.estado;
    }

    get Fotos(): Array<Foto> {
        return this.fotos;
    }

    get Danios(): Array<string> {
      return this.danios;
  }
}

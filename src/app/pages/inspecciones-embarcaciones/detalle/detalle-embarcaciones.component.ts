import { Component, OnInit } from '@angular/core';

import { hdiAnimations } from '../../../animations/index';
import { ActivatedRoute, Router } from '@angular/router';
import { InspeccionesEmbarcacionesService } from '../inspecciones-embarcaciones.service';
import { InspeccionEmbarcacion } from '../inspeccion-embarcacion.model';
import { ImagenPipe } from 'src/app/shared/pipes/imagen/imagen.pipe';

@Component({
    selector: 'app-detalle-embarcaciones',
    templateUrl: './detalle-embarcaciones.component.html',
    styleUrls: ['./detalle-embarcaciones.component.scss'],
    animations: hdiAnimations,
    providers: [ ImagenPipe ]
})
export class DetalleEmbarcacionesComponent implements OnInit {
    public inspeccion: InspeccionEmbarcacion;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _inspeccionesEmbarcacionesService: InspeccionesEmbarcacionesService
    ) {
        
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(params => {
            const id = params['id'];
            this._inspeccionesEmbarcacionesService.getInspeccion(id).subscribe(
                inspeccion => {
                    this.inspeccion = inspeccion;
                },
                error => {

                }
            )
        });
    }

    contieneElemento(type: string) {
        return this.inspeccion.Elementos.includes(type);
    }

    contieneInstrumento(type: string) {
        return this.inspeccion.Instrumentos.includes(type);
    }
}
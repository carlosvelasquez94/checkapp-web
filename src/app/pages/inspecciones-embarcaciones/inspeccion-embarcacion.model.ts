import { Foto } from 'src/app/shared/models/foto.model';
import { DanioEmbarcacion } from './danio-embarcacion.model';

export class InspeccionEmbarcacion {
    private id: string;
    private fecha: Date;
    private hora: string;
    private nombre: string;
    private telefono: string;
    private email: string;
    private reyJurisdiccion: string;
    private anio: string;
    private estado: string;
    private tipoEmbarcacion: string;
    private modelo: string;
    private astillero: string;
    private sumaValuada: string;
    private motorModelo: string;
    private motorCapacidad: string;
    private motorAnio: string;
    private nombreEmbarcacion: string;
    private fotos: Array<Foto>;
    private elementos: Array<String>;
    private instrumentos: Array<String>;
    private danios: DanioEmbarcacion;
    private documentacion: Array<Foto>;
    private auxiliar: Array<Foto>;
    private moneda: string;

    constructor (obj) {
        this.id = obj._id || '';
        this.fecha = obj.fecha || '';
        this.hora = obj.hora || '';
        this.nombre = obj.nombre || '';
        this.telefono = obj.telefono || '';
        this.email = obj.email || '';
        this.reyJurisdiccion = obj.reyjurisdiccion || '';
        this.anio = obj.anio || '';
        this.estado = obj.estado || '';
        this.tipoEmbarcacion = obj.tipoEmbarcacion || '';
        this.modelo = obj.modelo || '';
        this.astillero = obj.astillero || '';
        this.sumaValuada = obj.sumaValuada || '';
        this.motorModelo = obj.motorModelo || '';
        this.motorCapacidad = obj.motorCapacidad || '';
        this.motorAnio = obj.motorAnio || '';
        this.nombreEmbarcacion = obj.nombreEmbarcacion || '';
        this.moneda = obj.moneda || '';

        if (obj.danios) {
            this.danios = new DanioEmbarcacion(obj.danios);
        } else {
            this.danios = new DanioEmbarcacion({});
        }
        
        this.fotos = new Array<Foto>();
        if (obj.fotos.length > 0 ) {
            
            for(let foto of obj.fotos) {
                this.fotos.push(new Foto(foto))
            }
        }

        this.elementos = new Array<String>();
        if (obj.elementos.length > 0) {
            this.elementos = obj.elementos;
        }

        this.instrumentos = new Array<String>();
        if (obj.instrumentos.length > 0) {
            this.instrumentos = obj.instrumentos;
        }

        this.documentacion = new Array<Foto>();
        if (obj.documentacion.length > 0 ) {
            
            for(let documentacion of obj.documentacion) {
                this.documentacion.push(new Foto(documentacion))
            }
        }

        this.auxiliar = new Array<Foto>();
        if (obj.auxiliar.length > 0 ) {
            
            for(let auxiliar of obj.auxiliar) {
                this.auxiliar.push(new Foto(auxiliar))
            }
        }
    }

    get Id(): string {
        return this.id;
    }

    get Fecha(): Date {
        return this.fecha;
    }

    get Hora(): string {
        return this.hora;
    }

    get Nombre(): string {
        return this.nombre;
    }

    get Telefono(): string {
        return this.telefono;
    }

    get Email(): string {
        return this.email;
    }

    get ReyJurisdiccion(): string {
        return this.reyJurisdiccion;
    }

    get Anio(): string {
        return this.anio;
    }

    get Estado(): string {
        return this.estado;
    }

    get TipoEmbarcacion(): string {
        return this.tipoEmbarcacion;
    }

    get Modelo(): string {
        return this.modelo;
    }

    get Astillero(): string {
        return this.astillero;
    }

    get SumaValuada(): string {
        return this.sumaValuada;
    }

    get MotorModelo(): string {
        return this.motorModelo;
    }

    get MotorCapacidad(): string {
        return this.motorCapacidad;
    }

    get MotorAnio(): string {
        return this.motorAnio;
    }

    get NombreEmbarcacion(): string {
        return this.nombreEmbarcacion;
    }

    get Fotos(): Array<Foto> {
        return this.fotos;
    }

    get Elementos(): Array<String> {
        return this.elementos;
    }

    get Instrumentos(): Array<String> {
        return this.instrumentos;
    }

    get Danios(): DanioEmbarcacion {
        return this.danios;
    }

    get Documentacion(): Array<Foto> {
        return this.documentacion;
    }

    get Auxiliar(): Array<Foto> {
        return this.auxiliar;
    }

    get Moneda(): string {
        return this.moneda;
    }
} 
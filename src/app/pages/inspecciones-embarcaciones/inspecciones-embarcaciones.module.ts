import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import { 
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatListModule,
    MatSortModule
} from '@angular/material';

import { InspeccionesEmbarcacionesRoutingModule } from './inspecciones-embarcaciones-routing.module'
import { InspeccionesEmbarcacionesComponent } from './inspecciones-embarcaciones.component';
import { ListadoEmbarcacionesComponent } from './listado/listado-embarcaciones.component';
import { DetalleEmbarcacionesComponent } from './detalle/detalle-embarcaciones.component';

import { HeaderModule } from '../../components/header/header.module';
import { ImagenPipeModule } from '../../shared/pipes/imagen/imagen.module';

@NgModule({
    declarations: [
        InspeccionesEmbarcacionesComponent,
        ListadoEmbarcacionesComponent,
        DetalleEmbarcacionesComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        InspeccionesEmbarcacionesRoutingModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTabsModule,
        MatCardModule,
        MatInputModule,
        MatSlideToggleModule,
        MatDividerModule,
        MatListModule,
        MatSortModule,
        HeaderModule,
        ImagenPipeModule
    ]

})
export class InspeccionesEmbarcacionesModule { }
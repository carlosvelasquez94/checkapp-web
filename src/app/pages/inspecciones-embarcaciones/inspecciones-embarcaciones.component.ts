import { Component, OnInit } from "@angular/core";

import { hdiAnimations } from '../../animations/index';

@Component({
    selector: 'app-inspecciones-embarcaciones',
    templateUrl: './inspecciones-embarcaciones.component.html',
    styleUrls: ['./inspecciones-embarcaciones.component.scss'],
    animations: hdiAnimations
})
export class InspeccionesEmbarcacionesComponent implements OnInit {
    constructor(
    ) { 

    }

    ngOnInit(): void {

    }
}
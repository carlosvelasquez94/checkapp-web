export class DanioEmbarcacion {
    private motor: string;
    private motorAuxiliar: string;
    private presentaDanioEstructural: string;
    private presentaDanioVelas: string;

    constructor(obj) {
        this.motor = obj.motor || '';
        this.motorAuxiliar = obj.motorAuxiliar || '';
        this.presentaDanioEstructural = obj.presentaDanioEstructural || '';
        this.presentaDanioVelas = obj.presentaDanioVelas || '';
    }

    get Motor(): string {
        return this.motor;
    }

    get MotorAuxiliar(): string {
        return this.motorAuxiliar;
    }

    get PresentaDanioEstructural(): string {
        return this.presentaDanioEstructural
    }

    get PresentaDanioVelas(): string {
        return this.presentaDanioVelas;
    }
}
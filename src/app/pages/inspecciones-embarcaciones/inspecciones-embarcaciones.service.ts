import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { InspeccionEmbarcacion } from './inspeccion-embarcacion.model';


@Injectable({
    providedIn: 'root'
})
export class InspeccionesEmbarcacionesService {
    constructor(
        private _http: HttpClient,
    ){ }

    getInspecciones() {
        return this._http.get(environment.urlService + '/api/inspeccionEmbarcacion').pipe(
            map((data:any) => {
                return data.map(inspeccion => {
                    return new InspeccionEmbarcacion(inspeccion);
                });
            })
        );
    }

    getInspeccion(id: string) {
        return this._http.get(environment.urlService + '/api/inspeccionEmbarcacion/' + id).pipe(
            map((data:any) => {
                console.log(data);
                return new InspeccionEmbarcacion(data);
            })
        )
    }
}
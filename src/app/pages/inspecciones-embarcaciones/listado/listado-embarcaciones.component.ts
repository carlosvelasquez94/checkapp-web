import { Component, OnInit, ViewChild } from '@angular/core';

import { hdiAnimations } from '../../../animations/index';
import { InspeccionesEmbarcacionesService } from '../inspecciones-embarcaciones.service';
import { InspeccionEmbarcacion } from '../inspeccion-embarcacion.model';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector: 'app-inspecciones-emnbarcaciones',
    templateUrl: './listado-embarcaciones.component.html',
    styleUrls: ['./listado-embarcaciones.component.scss'],
    animations: hdiAnimations
})
export class ListadoEmbarcacionesComponent implements OnInit {
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    public inspecciones: MatTableDataSource<InspeccionEmbarcacion>;
    public displayColumns: string[] = ['fecha','fecha servidor', 'nombre','reyJurisdiccion','anio', 'estado', 'actions'];

    constructor(
        private _inspeccionesEmbarcacionesService: InspeccionesEmbarcacionesService,
        private _router: Router
    ) { }

    ngOnInit(): void {
        this._inspeccionesEmbarcacionesService.getInspecciones().subscribe(
            data => {
                this.inspecciones = new MatTableDataSource<InspeccionEmbarcacion>(data);
                this.inspecciones.sort = this.sort;
                this.inspecciones.paginator = this.paginator;
            },
            error => {

            }
        )
    }

    verDetalle(id: string): void {
        this._router.navigate(['/pages/inspecciones-embarcaciones', id]);
    }

    applyFilter(filterValue: string) {
        this.inspecciones.filter = filterValue.trim().toLowerCase();
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InspeccionesEmbarcacionesComponent } from './inspecciones-embarcaciones.component';
import { ListadoEmbarcacionesComponent } from './listado/listado-embarcaciones.component';
import { DetalleEmbarcacionesComponent } from './detalle/detalle-embarcaciones.component';

const routes: Routes = [
    { 
        path: '', 
        component: InspeccionesEmbarcacionesComponent,
        children: [
            { path: '', component: ListadoEmbarcacionesComponent },
            { path: ':id', component: DetalleEmbarcacionesComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InspeccionesEmbarcacionesRoutingModule { }
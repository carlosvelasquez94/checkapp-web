import { Component, OnInit } from '@angular/core';
import { hdiAnimations } from 'src/app/animations';

@Component({
    selector: 'app-tipos-danio',
    templateUrl: './tipos-danio.component.html',
    styleUrls: ['./tipos-danio.component.scss'],
    animations: hdiAnimations
})
export class TiposDanioComponent implements OnInit {
    
    ngOnInit(): void {

    }
}
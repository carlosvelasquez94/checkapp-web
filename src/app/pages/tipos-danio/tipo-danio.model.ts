export class TipoDanio {
    private id: string;
    private descripcion: string;

    constructor(obj) {
        this.id = obj._id || obj.id || '';
        this.descripcion = obj.descripcion || '';
    }

    get Id(): string {
        return this.id;
    }

    get Descripcion(): string {
        return this.descripcion;
    }
}
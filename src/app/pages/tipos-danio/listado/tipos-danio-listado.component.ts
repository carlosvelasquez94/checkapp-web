import { Component, OnInit, ViewChild } from '@angular/core';
import { hdiAnimations } from '../../../animations/index';
import { TiposDanioService } from '../tipos-danio.service';
import { TipoDanio } from '../tipo-danio.model';
import { MatTableDataSource, MatSort, MatDialog, MatDialogRef } from '@angular/material';
import { TiposDanioFormComponent } from '../form/tipos-danio-form.component';
import { FormGroup } from '@angular/forms';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'app-tipos-danio-list',
    templateUrl: './tipos-danio-listado.component.html',
    styleUrls: ['./tipos-danio-listado.component.scss'],
    animations: hdiAnimations
})
export class TiposDanioListComponent implements OnInit {
    @ViewChild(MatSort, {static: false}) sort: MatSort;

    public tiposDanio: MatTableDataSource<TipoDanio>;
    public displayColumns: string[] = ['descripcion'];

    public dialogRef;
    public confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

    constructor(
        private _tiposDanioService: TiposDanioService,
        private _matDialog: MatDialog
    ) {}

    ngOnInit(): void {
        this.loadData();
    }

    applyFilter(filterValue: string) {
        this.tiposDanio.filter = filterValue.trim().toLowerCase();
    }

    createTipoDanio(): void {
        this.dialogRef = this._matDialog.open(TiposDanioFormComponent, {
            panelClass: 'danio-form-dialog',
            data      : {
                action: 'new',
            }
        });

        this.dialogCallback();

    }

    editTipoDanio(tipoDanio: TipoDanio): void {
        this.dialogRef = this._matDialog.open(TiposDanioFormComponent, {
            panelClass: 'danio-form-dialog',
            data      : {
                action: 'edit',
                tipoDanio: tipoDanio
            }
        });

        this.dialogCallback();
    }

    dialogCallback() {
        this.dialogRef.afterClosed().subscribe(response => {
            if ( !response ) {
                return;
            }
            const actionType: string = response[0];
            const formData: FormGroup = response[1];

            switch ( actionType ) {
                case 'new':
                    this._tiposDanioService.insertTipoDanio(new TipoDanio(formData.getRawValue())).subscribe(
                        result => {
                            this.loadData();
                        }
                    );
                    break;
                case 'save':
                    console.log(formData.getRawValue());
                    this._tiposDanioService.editTipoDanio(new TipoDanio(formData.getRawValue())).subscribe(
                        result => {
                            this.loadData();
                        }
                    );
                    break;
                case 'delete':
                    this.deleteTipoDanio(new TipoDanio(formData.getRawValue()));
                    break;
            }
        });
    }

    deleteTipoDanio(tipoDanio: TipoDanio): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = '¿Estás seguro que qures borrarlo?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result ) {
                this._tiposDanioService.deleteTipoDanio(tipoDanio).subscribe(
                    resp => {
                        this.loadData();
                    }
                )
            }
            this.confirmDialogRef = null;
        });

    }

    private loadData(): void {
        this._tiposDanioService.getTiposDanio().subscribe(
            data => {
                this.tiposDanio = new MatTableDataSource<TipoDanio>(data);
                this.tiposDanio.sort = this.sort;
            },
            error => {

            }
        )
    }
} 
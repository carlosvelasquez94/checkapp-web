import { Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { hdiAnimations } from '../../../animations/index';
import { TipoDanio } from '../tipo-danio.model';

@Component({
    selector: 'app-tipos-danio-form',
    templateUrl: './tipos-danio-form.component.html',
    styleUrls: ['./tipos-danio-form.component.scss'],
    animations: hdiAnimations
})
export class TiposDanioFormComponent {
    public tipoDanio: TipoDanio;
    public action: string;
    public tipoDanioForm: FormGroup;
    public dialogTitle: string;

    public estados: String[] = ['CHICO (MENOS DE 5CM)', 'GRANDE (MAS DE 5CM)', 'SIN DESCRIPCIÓN'];

    constructor(
        public matDialogRef: MatDialogRef<TiposDanioFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ){ 
        this.action = this._data.action;
        if ( this.action === 'edit' ) {
            this.dialogTitle = 'Editar Tipo Daño';
            this.tipoDanio = _data.tipoDanio;
        } else {
            this.dialogTitle = 'Crear Tipo Daño';
            this.tipoDanio = new TipoDanio({});
        }
        this.tipoDanioForm = this.createTipoDanioForm();
    }

    createTipoDanioForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.tipoDanio.Id],
            descripcion: [this.tipoDanio.Descripcion]
        });
    }
}
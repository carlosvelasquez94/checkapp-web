import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';

import { 
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTabsModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule
} from '@angular/material';

import { TiposDanioComponent } from './tipos-danio.component';
import { TiposDanioRoutingModule } from './tipos-danio-routing.module';
import { TiposDanioListComponent } from './listado/tipos-danio-listado.component';
import { TiposDanioFormComponent } from './form/tipos-danio-form.component';

import { HeaderModule } from '../../components/header/header.module';
import { ConfirmDialogModule } from '../../components/confirm-dialog/confirm-dialog.module';

@NgModule({
    declarations: [
        TiposDanioComponent,
        TiposDanioListComponent,
        TiposDanioFormComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        TiposDanioRoutingModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTabsModule,
        MatInputModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        HeaderModule,
        ConfirmDialogModule
    ],
    entryComponents: [
        TiposDanioFormComponent
    ]

})
export class TiposDanioModule { }
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { TipoDanio } from './tipo-danio.model';

@Injectable({
    providedIn: 'root'
})
export class TiposDanioService {
    constructor(
        private _http: HttpClient,
    ){ }

    getTiposDanio() {
        return this._http.get(environment.urlService + '/api/tipoDanio').pipe(
            map((data:any) => {
                return data.map(inspeccion => {
                    return new TipoDanio(inspeccion);
                });
            })
        );
    }

    insertTipoDanio(tipoDanio: TipoDanio) {
        return this._http.post(environment.urlService + '/api/tipoDanio', tipoDanio);
    }

    deleteTipoDanio(tipoDanio: TipoDanio) {
        return this._http.delete(environment.urlService + `/api/tipoDanio/${tipoDanio.Id}`);
    }

    editTipoDanio(tipoDanio: TipoDanio) {
        return this._http.put(environment.urlService + `/api/tipoDanio/${tipoDanio.Id}`, tipoDanio);
    }
}
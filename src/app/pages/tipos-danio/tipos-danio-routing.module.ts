import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiposDanioComponent } from './tipos-danio.component';
import { TiposDanioListComponent } from './listado/tipos-danio-listado.component';



const routes: Routes = [
    { 
        path: '', component: TiposDanioComponent,
        children: [
            { path: '', component: TiposDanioListComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TiposDanioRoutingModule { }
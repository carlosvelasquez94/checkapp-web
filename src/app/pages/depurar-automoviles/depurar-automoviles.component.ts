import { Component } from "@angular/core";

import { hdiAnimations } from '../../animations/index';
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from '../../shared/adapter/date.adapter'
import { DepurarAutomovilesService } from './depurar-automoviles.service';

@Component({
    selector: 'app-depurar-automoviles',
    templateUrl: './depurar-automoviles.component.html',
    styleUrls: ['./depurar-automoviles.component.scss'],
    animations: hdiAnimations,
    providers: [
        { provide: DateAdapter, useClass: AppDateAdapter },
        { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
    ]
})
export class DepurarAutomovilesComponent {
    public fechaHasta;

    constructor(
        private _depurarAutomovilesService: DepurarAutomovilesService
    ){ }

    depurar() {
        let payload = {
            hasta: this.fechaHasta
        };

        this._depurarAutomovilesService.depurar(payload).subscribe(
            result => {

            },
            error => {
                
            }
        )
        
    }

    onDateHasta(event) {
        this.fechaHasta = event.targetElement.value
    }
}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DepurarAutomovilesComponent } from './depurar-automoviles.component';

const routes: Routes = [
  { path: '', component: DepurarAutomovilesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepurarAutomovilesRoutingModule { }
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class DepurarAutomovilesService {
    constructor(
        private _http: HttpClient,
    ){ }

    depurar(payload) {
        return this._http.post(environment.urlService + '/api/inspeccion/depurar',payload);
    }
}
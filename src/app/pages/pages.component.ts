import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit } from "@angular/core";

import { hdiAnimations } from '../animations/index';
import { MenuItem } from '../components/menu-item/menu-item';
import { MenuItemService } from '../components/menu-item/menu-item.service';

@Component({
    selector: 'app-pages',
    templateUrl: './pages.component.html',
    styleUrls: ['./pages.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: hdiAnimations
})
export class PagesComponent implements OnInit, AfterViewInit {
    @ViewChild('appDrawer', {static: false}) appDrawer: ElementRef;

    menuItems: MenuItem[] = [
        {
            displayName: 'Automoviles',
            iconName: 'hdi_car',
            route: 'automoviles',
            children: [
                {
                    displayName: 'Inspecciones',
                    iconName: 'hdi_inspeccion',
                    route: '/pages/inspecciones-automoviles',
                },
                {
                    displayName: 'Tipos Daño',
                    iconName: 'hdi_danios_1',
                    route: '/pages/tipos-danio',
                },
                {
                    displayName: 'Daños',
                    iconName: 'hdi_danios_3',
                    route: '/pages/danios',
                },
                {
                    displayName: 'Coberturas',
                    iconName: 'hdi_cobertura',
                    route: '/pages/coberturas',
                },
                {
                    displayName: 'Reporte',
                    iconName: 'hdi_reporte',
                    route: '/pages/reporte-automoviles',
                },
                {
                    displayName: 'Depurar',
                    iconName: '',
                    route: '/pages/depurar-automoviles',
                },
            ]
        },
        {
            displayName: 'Embarcaciones',
            iconName: 'hdi_embarcacion',
            route: 'devfestfl',
            children: [
                {
                    displayName: 'Inspecciones',
                    iconName: 'hdi_inspeccion',
                    route: '/pages/inspecciones-embarcaciones',
                }
            ]
        },
        {
            displayName: 'Siniestros',
            iconName: 'hdi_danios_2',
            route: 'devfestfl',
            children: [
                {
                    displayName: 'Inspecciones',
                    iconName: 'hdi_inspeccion',
                    route: '/pages/inspecciones-siniestros',
                }
            ]
        }
    ];
    
    constructor(
        private _menuItemService: MenuItemService
    ) { }

    ngOnInit(): void {

    }

    ngAfterViewInit() {
        this._menuItemService.appDrawer = this.appDrawer;
    }
}
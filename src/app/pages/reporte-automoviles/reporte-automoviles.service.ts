import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class ReporteAutomovilesService {
    constructor(
        private _http: HttpClient,
    ){ }

    download(payload) {
        return this._http.post(environment.urlService + '/api/inspeccion/generateExcel',payload, { responseType: 'blob'}).pipe(
            map(res => {
                return {
                    filename: `reporte-${new Date().getTime()}.xlsx`,
                    data: res
                };
            })
        );
    }
}
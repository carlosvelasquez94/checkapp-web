import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReporteAutomovilesComponent } from './reporte-automoviles.component';

const routes: Routes = [
  { path: '', component: ReporteAutomovilesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReporteAutomovilesRoutingModule { }

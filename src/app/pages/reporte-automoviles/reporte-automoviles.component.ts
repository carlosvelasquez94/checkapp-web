import { Component } from "@angular/core";

import { hdiAnimations } from '../../animations/index';
import { ReporteAutomovilesService } from './reporte-automoviles.service';
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from '../../shared/adapter/date.adapter'

@Component({
    selector: 'app-reporte-automoviles',
    templateUrl: './reporte-automoviles.component.html',
    styleUrls: ['./reporte-automoviles.component.scss'],
    animations: hdiAnimations,
    providers: [
        { provide: DateAdapter, useClass: AppDateAdapter },
        { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
    ]
})
export class ReporteAutomovilesComponent {
    public fechaDesde;
    public fechaHasta;

    constructor(
        private _reporteService: ReporteAutomovilesService
    ){ }

    public mask = {
        guide: true,
        showMask : true,
        mask: [/\d/, /\d/, '/', /\d/, /\d/, '/',/\d/, /\d/,/\d/, /\d/]
    };

    download() {
        let payload = {
            desde: this.fechaDesde,
            hasta: this.fechaHasta
        };
        this._reporteService.download(payload).subscribe(data => {
            this.downloadFile(data);
        },
        error => {
            console.log(error);
        })
    }

    downloadFile(res) {
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
    };

    onDateHasta(event) {
        this.fechaHasta = event.targetElement.value
    }

    onDateDesde(event) {
        this.fechaDesde = event.targetElement.value
    }
}
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';

import {
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatSortModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule
} from '@angular/material';

import { ReporteAutomovilesRoutingModule } from './reporte-automoviles-routing.module';
import { ReporteAutomovilesComponent } from './reporte-automoviles.component';

import { HeaderModule } from '../../components/header/header.module';


@NgModule({
    declarations: [
        ReporteAutomovilesComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        HttpClientModule,
        ReporteAutomovilesRoutingModule,
        FlexLayoutModule,
        MatIconModule,
        MatButtonModule,
        MatTableModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatTabsModule,
        MatSortModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatNativeDateModule,
        HeaderModule,
        TextMaskModule
    ]

})
export class ReporteAutomovilesModule { }
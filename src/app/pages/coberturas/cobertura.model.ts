export class Cobertura {
    private id: string;
    private codigo: string;
    private descripcion: string;
    private valor: string;
    
    constructor(obj) {
        this.id = obj._id || obj.id || '';
        this.codigo = obj.codigo || '';
        this.descripcion = obj.descripcion || '';
        this.valor = obj.valor || '';
    }

    get Id(): string {
        return this.id;
    }

    get Codigo(): string {
        return this.codigo;
    }

    get Descripcion(): string {
        return this.descripcion;
    }

    get Valor(): string {
        return this.valor;
    }
}
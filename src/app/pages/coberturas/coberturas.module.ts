import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';

import { 
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTabsModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatOptionModule,
    MatSelectModule
} from '@angular/material';

import { CoberturasComponent } from './coberturas.component';
import { CoberturasListComponent } from './listado/coberturas-listado.component';
import { CoberturaFormComponent } from './form/cobertura-form.component';
import { CoberturasRoutingModule } from './coberturas-routing.module';

import { HeaderModule } from '../../components/header/header.module';
import { ConfirmDialogModule } from '../../components/confirm-dialog/confirm-dialog.module';

@NgModule({
    declarations: [
        CoberturasComponent,
        CoberturasListComponent,
        CoberturaFormComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        CoberturasRoutingModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTabsModule,
        MatInputModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        MatOptionModule,
        MatSelectModule,
        HeaderModule,
        ConfirmDialogModule
    ],
    entryComponents: [
        CoberturaFormComponent
    ]

})
export class CoberturasModule { }
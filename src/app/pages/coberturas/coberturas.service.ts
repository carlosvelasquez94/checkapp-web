import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Cobertura } from './cobertura.model';

import { environment } from '../../../environments/environment.prod';


@Injectable({
    providedIn: 'root'
})
export class CoberturasService {
    constructor(
        private _http: HttpClient,
    ){ }

    getCoberturas() {
        return this._http.get(environment.urlService + '/api/cobertura').pipe(
            map((data:any) => {
                return data.map(cobertura => {
                    return new Cobertura(cobertura);
                });
            })
        );
    }

    insertCobertura(cobertura: Cobertura) {
        return this._http.post(environment.urlService + '/api/cobertura', cobertura);
    }

    deleteCobertura(cobertura: Cobertura) {
        return this._http.delete(environment.urlService + `/api/cobertura/${cobertura.Id}`);
    }

    editCobertura(cobertura: Cobertura) {
        return this._http.put(environment.urlService + `/api/cobertura/${cobertura.Id}`, cobertura);
    }
}
import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { hdiAnimations } from '../../../animations/index';
import { Cobertura } from '../cobertura.model';

@Component({
    selector: 'app-coberturas-form',
    templateUrl: './cobertura-form.component.html',
    styleUrls: ['./cobertura-form.component.scss'],
    animations: hdiAnimations
})
export class CoberturaFormComponent implements OnInit {
    public cobertura: Cobertura;
    public action: string;
    public coberturaForm: FormGroup;
    public dialogTitle: string;

    constructor(
        public matDialogRef: MatDialogRef<CoberturaFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ){ 
        this.action = this._data.action;
        if ( this.action === 'edit' ) {
            this.dialogTitle = 'Editar Cobertura';
            this.cobertura = _data.cobertura;
        } else {
            this.dialogTitle = 'Crear Cobertura';
            this.cobertura = new Cobertura({});
        }
        this.coberturaForm = this.createCoberturaForm();
    }

    ngOnInit() {
       
    }

    createCoberturaForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.cobertura.Id],
            codigo: [this.cobertura.Codigo],
            descripcion: [this.cobertura.Descripcion],
            valor: [this.cobertura.Valor],
        });
    }
}
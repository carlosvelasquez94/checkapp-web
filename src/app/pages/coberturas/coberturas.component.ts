import { Component, OnInit } from '@angular/core';
import { hdiAnimations } from 'src/app/animations';

@Component({
    selector: 'app-coberturas',
    templateUrl: './coberturas.component.html',
    styleUrls: ['./coberturas.component.scss'],
    animations: hdiAnimations
})
export class CoberturasComponent implements OnInit {
    
    ngOnInit(): void {

    }
}
import { Component, ViewChild, OnInit } from '@angular/core';
import { hdiAnimations } from 'src/app/animations';
import { MatSort, MatDialog, MatTableDataSource, MatDialogRef } from '@angular/material';

import { CoberturasService } from '../coberturas.service';
import { Cobertura } from '../cobertura.model';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';
import { CoberturaFormComponent } from '../form/cobertura-form.component';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-coberturas-list',
    templateUrl: './coberturas-listado.component.html',
    styleUrls: ['./coberturas-listado.component.scss'],
    animations: hdiAnimations
})
export class CoberturasListComponent implements OnInit {
    @ViewChild(MatSort, {static: false}) sort: MatSort;

    public coberturas: MatTableDataSource<Cobertura>;
    public displayColumns: string[] = ['codigo','descripcion','valor'];

    public dialogRef;
    public confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

    constructor(
        private _matDialog: MatDialog,
        private _coberturasService: CoberturasService
    ) { }

    ngOnInit(): void {
        this.loadData();
    }

    applyFilter(filterValue: string) {
        this.coberturas.filter = filterValue.trim().toLowerCase();
    }

    createCobertura(): void {
        this.dialogRef = this._matDialog.open(CoberturaFormComponent, {
            panelClass: 'danio-form-dialog',
            data      : {
                action: 'new',
            }
        });

       this.dialogCallback();
    }

    editCobertura(cobertura: Cobertura): void {
        this.dialogRef = this._matDialog.open(CoberturaFormComponent, {
            panelClass: 'danio-form-dialog',
            data      : {
                action: 'edit',
                cobertura: cobertura
            }
        });

        this.dialogCallback();
    }

    dialogCallback() {
        this.dialogRef.afterClosed().subscribe(response => {
            if ( !response ) {
                return;
            }
            const actionType: string = response[0];
            const formData: FormGroup = response[1];

            switch ( actionType ) {
                case 'new':
                    this._coberturasService.insertCobertura(formData.getRawValue()).subscribe(
                        result => {
                            this.loadData();
                        }
                    );
                    break;

                case 'save':
                    this._coberturasService.editCobertura(new Cobertura(formData.getRawValue())).subscribe(
                        result => {
                            this.loadData();
                        }
                    );
                    break;

                case 'delete':
                    this.deleteCobertura(new Cobertura(formData.getRawValue()));
                    break;
            }
        });
    }

    deleteCobertura(cobertura: Cobertura): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = '¿Estás seguro que qures borrarlo?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result ) {
                this._coberturasService.deleteCobertura(cobertura).subscribe(
                    resp => {
                        this.loadData();
                    }
                )
            }
            this.confirmDialogRef = null;
        });

    }
    
    private loadData(): void {
        this._coberturasService.getCoberturas().subscribe(
            data => {
                this.coberturas = new MatTableDataSource<Cobertura>(data);
                this.coberturas.sort = this.sort;
            },
            error => {

            }
        );
    }


}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoberturasComponent } from './coberturas.component';
import { CoberturasListComponent } from './listado/coberturas-listado.component';

const routes: Routes = [
    { 
        path: '', component: CoberturasComponent,
        children: [
            { path: '', component: CoberturasListComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoberturasRoutingModule { }
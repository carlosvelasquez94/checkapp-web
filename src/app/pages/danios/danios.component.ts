import { Component, OnInit } from '@angular/core';
import { hdiAnimations } from 'src/app/animations';

@Component({
    selector: 'app-danios',
    templateUrl: './danios.component.html',
    styleUrls: ['./danios.component.scss'],
    animations: hdiAnimations
})
export class DaniosComponent implements OnInit {
    
    ngOnInit(): void {

    }
}
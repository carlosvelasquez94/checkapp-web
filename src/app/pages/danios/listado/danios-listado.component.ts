import { Component, OnInit, ViewChild } from '@angular/core';
import { hdiAnimations } from 'src/app/animations';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource, MatDialogRef } from '@angular/material';

import { DaniosService } from '../danios.service';
import { Danio } from '../danio.model';
import { DaniosFormComponent } from '../form/danio-form.component';
import { FormGroup } from '@angular/forms';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'app-danios-list',
    templateUrl: './danios-listado.component.html',
    styleUrls: ['./danios-listado.component.scss'],
    animations: hdiAnimations
})
export class DaniosListComponent implements OnInit {
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    
    public danios: MatTableDataSource<Danio>;
    public displayColumns: string[] = ['autoparte', 'danio', 'estado'];

    public dialogRef;
    public confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

    constructor(
        private _matDialog: MatDialog,
        private _daniosService: DaniosService
    ) { }

    ngOnInit(): void {
        this.loadData();
    }

    applyFilter(filterValue: string) {
        this.danios.filter = filterValue.trim().toLowerCase();
    }

    createDanio(): void {
        this.dialogRef = this._matDialog.open(DaniosFormComponent, {
            panelClass: 'danio-form-dialog',
            data      : {
                action: 'new',
            }
        });

       this.dialogCallback();
    }

    editDanio(danio: Danio): void {
        this.dialogRef = this._matDialog.open(DaniosFormComponent, {
            panelClass: 'danio-form-dialog',
            data      : {
                action: 'edit',
                danio: danio
            }
        });

        this.dialogCallback();
    }

    dialogCallback() {
        this.dialogRef.afterClosed().subscribe(response => {
            if ( !response ) {
                return;
            }
            const actionType: string = response[0];
            const formData: FormGroup = response[1];

            switch ( actionType ) {
                case 'new':
                    this._daniosService.insertDanio(formData.getRawValue()).subscribe(
                        result => {
                            this.loadData();
                        }
                    );
                    break;

                case 'save':
                    this._daniosService.editDanio(new Danio(formData.getRawValue())).subscribe(
                        result => {
                            this.loadData();
                        }
                    );
                    break;

                case 'delete':
                    this.deleteDanio(new Danio(formData.getRawValue()));
                    break;
            }
        });
    }

    deleteDanio(danio: Danio): void {
        this.confirmDialogRef = this._matDialog.open(ConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = '¿Estás seguro que qures borrarlo?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result ) {
                this._daniosService.deleteDanio(danio).subscribe(
                    resp => {
                        this.loadData();
                    }
                )
            }
            this.confirmDialogRef = null;
        });

    }

    private loadData(): void {
        this._daniosService.getDanios().subscribe(
            data => {
                this.danios = new MatTableDataSource<Danio>(data);
                this.danios.sort = this.sort;
                this.danios.paginator = this.paginator;
            },
            error => {

            }
        );
    }
}
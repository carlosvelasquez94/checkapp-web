import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaniosComponent } from './danios.component';
import { DaniosListComponent } from './listado/danios-listado.component';



const routes: Routes = [
    { 
        path: '', component: DaniosComponent,
        children: [
            { path: '', component: DaniosListComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DaniosRoutingModule { }
import { Autoparte } from '../autoparte/autoparte.model';
import { TipoDanio } from '../tipos-danio/tipo-danio.model';

export class Danio {
    private id: string;
    private clave: string;
    private danio: TipoDanio;
    private estado: string;
    private codigoGaus: string;
    private descripcionGaus: string;
    private autoparte: Autoparte;
    private valor: string;

    constructor(obj) {
        this.id = obj._id || obj.id || '';
        this.clave = obj.clave || '';
        this.danio = obj.danio || '';
        this.estado = obj.estado || '';
        this.codigoGaus = obj.codigoGaus || '';
        this.descripcionGaus = obj.descripcionGaus || '';
        this.valor = obj.valor || '';

        if (obj.danio) {
            this.danio = new TipoDanio(obj.danio);
        } else {
            this.danio = new TipoDanio({});
        }
        if (obj.autoparte) {
            this.autoparte = new Autoparte(obj.autoparte)
        } else {
            this.autoparte = new Autoparte({});
        }
    }

    get Id(): string {
        return this.id;
    }

    get Clave(): string {
        return this.clave;
    }

    get Danio(): TipoDanio {
        return this.danio;
    }

    get Estado(): string {
        return this.estado;
    }

    get CodigoGaus(): string {
        return this.codigoGaus;
    }

    get DescripcionGaus(): string {
        return this.descripcionGaus;
    }

    get Autoparte(): Autoparte {
        return this.autoparte;
    }

    get Valor(): string {
        return this.valor;
    }
}
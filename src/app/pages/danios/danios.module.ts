import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';

import { 
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTabsModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatOptionModule,
    MatSelectModule
} from '@angular/material';

import { DaniosComponent } from './danios.component';
import { DaniosListComponent } from './listado/danios-listado.component';
import { DaniosFormComponent } from './form/danio-form.component';
import { DaniosRoutingModule } from './danios-routing.module';

import { HeaderModule } from '../../components/header/header.module';
import { ConfirmDialogModule } from '../../components/confirm-dialog/confirm-dialog.module';

@NgModule({
    declarations: [
        DaniosComponent,
        DaniosListComponent,
        DaniosFormComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        DaniosRoutingModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTabsModule,
        MatInputModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        MatOptionModule,
        MatSelectModule,
        HeaderModule,
        ConfirmDialogModule
    ],
    entryComponents: [
        DaniosFormComponent
    ]

})
export class DaniosModule { }
import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { hdiAnimations } from '../../../animations/index';
import { Danio } from '../danio.model';
import { Autoparte } from '../../autoparte/autoparte.model';
import { TipoDanio } from "../../tipos-danio/tipo-danio.model";
import { AutopartesService } from '../../autoparte/autoparte.service';
import { TiposDanioService } from '../../tipos-danio/tipos-danio.service';

@Component({
    selector: 'app-danios-form',
    templateUrl: './danio-form.component.html',
    styleUrls: ['./danio-form.component.scss'],
    animations: hdiAnimations
})
export class DaniosFormComponent implements OnInit {
    public danio: Danio;
    public autopartes: Autoparte[];
    public tiposDanio: TipoDanio[];
    public action: string;
    public danioForm: FormGroup;
    public dialogTitle: string;

    public estados: String[] = ['CHICO (MENOS DE 5CM)', 'GRANDE (MAS DE 5CM)', 'SIN DESCRIPCIÓN'];

    constructor(
        private _autopartesService: AutopartesService,
        private _tipoDanioService: TiposDanioService,
        public matDialogRef: MatDialogRef<DaniosFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ){ 
        this.action = this._data.action;
        if ( this.action === 'edit' ) {
            this.dialogTitle = 'Editar Daño';
            this.danio = _data.danio;
        } else {
            this.dialogTitle = 'Crear Daño';
            this.danio = new Danio({});
        }
        this.danioForm = this.createDanioForm();
    }

    ngOnInit() {
        this._tipoDanioService.getTiposDanio().subscribe(
            data => {
                this.tiposDanio = data;
            }
        );

        this._autopartesService.getAutopartes().subscribe(
            data => {
                this.autopartes = data;
            }
        )
        
    }

    createDanioForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.danio.Id],
            autoparte: [this.danio.Autoparte.Id],
            clave: [this.danio.Clave],
            danio: [this.danio.Danio.Id],
            estado: [this.danio.Estado],
            codigoGaus: [this.danio.CodigoGaus],
            descripcionGaus: [this.danio.DescripcionGaus],
            valor: [this.danio.Valor]
        });
    }
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Danio } from './danio.model';

import { environment } from '../../../environments/environment.prod';


@Injectable({
    providedIn: 'root'
})
export class DaniosService {
    constructor(
        private _http: HttpClient,
    ){ }

    getDanios() {
        return this._http.get(environment.urlService + '/api/danio').pipe(
            map((data:any) => {
                return data.map(danio => {
                    return new Danio(danio);
                });
            })
        );
    }

    insertDanio(danio: Danio) {
        return this._http.post(environment.urlService + '/api/danio', danio);
    }

    deleteDanio(danio: Danio) {
        return this._http.delete(environment.urlService + `/api/danio/${danio.Id}`);
    }

    editDanio(danio: Danio) {
        return this._http.put(environment.urlService + `/api/danio/${danio.Id}`, danio);
    }
}
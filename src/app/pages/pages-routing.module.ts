import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesComponent } from './pages.component';

const routes: Routes = [
    { 
        path: '', 
        component: PagesComponent,
        children: [
            { path: 'inspecciones-automoviles', loadChildren: './inspecciones-automoviles/inspecciones-automoviles.module#InspeccionesAutomovilesModule' },
            { path: 'inspecciones-embarcaciones', loadChildren: './inspecciones-embarcaciones/inspecciones-embarcaciones.module#InspeccionesEmbarcacionesModule' },
            { path: 'inspecciones-siniestros', loadChildren: './inspecciones-siniestros/inspecciones-siniestros.module#InspeccionesSiniestrosModule' },
            { path: 'danios', loadChildren: './danios/danios.module#DaniosModule' },
            { path: 'tipos-danio', loadChildren: './tipos-danio/tipos-danio.module#TiposDanioModule' },
            { path: 'coberturas', loadChildren: './coberturas/coberturas.module#CoberturasModule' },
            { path: 'reporte-automoviles', loadChildren: './reporte-automoviles/reporte-automoviles.module#ReporteAutomovilesModule' },
            { path: 'depurar-automoviles', loadChildren: './depurar-automoviles/depurar-automoviles.module#DepurarAutomovilesModule' },
            { path: '', redirectTo: '/pages/inspecciones-automoviles', pathMatch: 'full' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
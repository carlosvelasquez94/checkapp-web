import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSidenavModule, MatListModule } from '@angular/material';

import { PagesRoutingModule } from './pages-routing.module'
import { PagesComponent } from './pages.component';

import { MenuItemModule } from '../components/menu-item/menu-item.module';

@NgModule({
    declarations: [
        PagesComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        PagesRoutingModule,
        FlexLayoutModule,
        MatSidenavModule,
        MatListModule,
        MenuItemModule
    ]

})
export class PagesModule { }
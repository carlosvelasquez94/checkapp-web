import { Component, OnInit, ViewChild } from '@angular/core';

import { hdiAnimations } from '../../../animations/index';
import { InspeccionesAutomovilesService } from '../inspecciones-automoviles.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { InspeccionAutomovil } from '../inspeccion-automovil.model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-inspecciones-automoviles',
    templateUrl: './listado-automoviles.component.html',
    styleUrls: ['./listado-automoviles.component.scss'],
    animations: hdiAnimations
})
export class ListadoAutomovilesComponent implements OnInit {
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    public inspecciones: MatTableDataSource<InspeccionAutomovil>;
    public displayColumns: string[] = ['fecha', 'fecha servidor', 'nombre','patente','anio', 'estado', 'actions'];

    constructor(
        private _inspeccionesAutomovilesService: InspeccionesAutomovilesService,
        private _router: Router
    ) { }

    ngOnInit(): void {
        this._inspeccionesAutomovilesService.getInspecciones().subscribe(
            data => {
                this.inspecciones = new MatTableDataSource<InspeccionAutomovil>(data);
                this.inspecciones.sort = this.sort;
                this.inspecciones.paginator = this.paginator;
            },
            error => {

            }
        )
    }

    verDetalle(id: string): void {
        this._router.navigate(['/pages/inspecciones-automoviles', id]);
    }

    applyFilter(filterValue: string) {
        this.inspecciones.filter = filterValue.trim().toLowerCase();
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import { InspeccionAutomovil } from './inspeccion-automovil.model';

@Injectable({
    providedIn: 'root'
})
export class InspeccionesAutomovilesService {
    constructor(
        private _http: HttpClient,
    ){ }

    getInspecciones() {
        return this._http.get(environment.urlService + '/api/inspeccion').pipe(
            map((data:any) => {
                return data.map(inspeccion => {
                    return new InspeccionAutomovil(inspeccion);
                });
            })
        );
    }

    getInspeccion(id: string) {
        return this._http.get(environment.urlService + '/api/inspeccion/' + id).pipe(
            map((data:any) => {
                return new InspeccionAutomovil(data);
            })
        )
    }
}
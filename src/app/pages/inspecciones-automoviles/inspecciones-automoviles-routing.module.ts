import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InspeccionesAutomovilesComponent } from './inspecciones-automoviles.component';
import { ListadoAutomovilesComponent } from './listado/listado-automoviles.component';
import { DetalleAutomovilesComponent } from './detalle/detalle-automoviles.component';

const routes: Routes = [
    { 
        path: '', 
        component: InspeccionesAutomovilesComponent,
        children: [
            { path: '', component: ListadoAutomovilesComponent },
            { path: ':id', component: DetalleAutomovilesComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InspeccionesAutomovilesRoutingModule { }
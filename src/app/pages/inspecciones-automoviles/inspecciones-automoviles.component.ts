import { Component, OnInit } from "@angular/core";

import { hdiAnimations } from '../../animations/index';

@Component({
    selector: 'app-inspecciones-automoviles',
    templateUrl: './inspecciones-automoviles.component.html',
    styleUrls: ['./inspecciones-automoviles.component.scss'],
    animations: hdiAnimations
})
export class InspeccionesAutomovilesComponent implements OnInit {
    constructor(
    ) { 

    }

    ngOnInit(): void {

    }
}
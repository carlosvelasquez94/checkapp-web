import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import { 
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatSortModule
} from '@angular/material';

import { InspeccionesAutomovilesRoutingModule } from './inspecciones-automoviles-routing.module'
import { InspeccionesAutomovilesComponent } from './inspecciones-automoviles.component';
import { ListadoAutomovilesComponent } from './listado/listado-automoviles.component';
import { DetalleAutomovilesComponent } from './detalle/detalle-automoviles.component';

import { HeaderModule } from '../../components/header/header.module';
import { ImagenPipeModule } from '../../shared/pipes/imagen/imagen.module';



@NgModule({
    declarations: [
        InspeccionesAutomovilesComponent,
        ListadoAutomovilesComponent,
        DetalleAutomovilesComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        InspeccionesAutomovilesRoutingModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTabsModule,
        MatSortModule,
        MatCardModule,
        MatInputModule,
        HeaderModule,
        ImagenPipeModule
    ]

})
export class InspeccionesAutomovilesModule { }
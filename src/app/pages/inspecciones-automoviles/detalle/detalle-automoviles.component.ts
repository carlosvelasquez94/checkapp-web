import { Component, OnInit, ViewChild } from '@angular/core';

import { hdiAnimations } from '../../../animations/index';
import { InspeccionesAutomovilesService } from '../inspecciones-automoviles.service';
import { InspeccionAutomovil } from '../inspeccion-automovil.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ImagenPipe } from '../../../shared/pipes/imagen/imagen.pipe';

@Component({
    selector: 'app-detalle-automoviles',
    templateUrl: './detalle-automoviles.component.html',
    styleUrls: ['./detalle-automoviles.component.scss'],
    animations: hdiAnimations,
    providers: [ ImagenPipe ]
})
export class DetalleAutomovilesComponent implements OnInit {
    public inspeccion: InspeccionAutomovil;
    public displayColumnsDanios: string[] = ['autoparte','danio','estado'];

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _inspeccionesAutomovilesService: InspeccionesAutomovilesService
    ) { }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(params => {
            const id = params['id'];
            this._inspeccionesAutomovilesService.getInspeccion(id).subscribe(
                inspeccion => {
                    this.inspeccion = inspeccion;
                },
                error => {

                }
            )
        });
    }
}
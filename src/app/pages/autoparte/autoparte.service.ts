import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Autoparte } from './autoparte.model';

import { environment } from '../../../environments/environment.prod';


@Injectable({
    providedIn: 'root'
})
export class AutopartesService {
    constructor(
        private _http: HttpClient,
    ){ }

    getAutopartes() {
        return this._http.get(environment.urlService + '/api/autoparte').pipe(
            map((data:any) => {
                return data.map(autoparte => {
                    return new Autoparte(autoparte);
                });
            })
        );
    }
}
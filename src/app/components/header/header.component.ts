import { Component, OnInit, Input } from '@angular/core';
import { hdiAnimations } from '../../animations/index';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    animations: hdiAnimations
})
export class HeaderComponent implements OnInit {

    @Input() titulo: string = '';
    @Input() icon: string = '';

    ngOnInit(): void {
    }
    
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ConfirmDialogComponent } from './confirm-dialog.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { 
    MatIconModule,
    MatButtonModule,
    MatDialogModule
} from '@angular/material';

@NgModule({
    declarations: [ ConfirmDialogComponent ],
    exports: [ ConfirmDialogComponent ],
    imports: [ CommonModule, RouterModule, FlexLayoutModule, MatIconModule, MatButtonModule, MatDialogModule ],
    entryComponents: [ ConfirmDialogComponent ]
})

export class ConfirmDialogModule { }
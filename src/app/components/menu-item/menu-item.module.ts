import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MenuItemComponent } from './menu-item.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material';

@NgModule({
    declarations: [ MenuItemComponent ],
    exports: [MenuItemComponent ],
    imports: [ CommonModule, RouterModule, FlexLayoutModule, MatIconModule ]
})

export class MenuItemModule { }
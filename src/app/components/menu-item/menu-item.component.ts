import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MenuItem } from './menu-item';
import { Router } from '@angular/router';
import { MenuItemService } from './menu-item.service';

@Component({
    selector: 'app-menu-item',
    templateUrl: './menu-item.component.html',
    styleUrls: ['./menu-item.component.scss'],
    animations: [
        trigger('indicatorRotate', [
            state('collapsed', style({transform: 'rotate(0deg)'})),
            state('expanded', style({transform: 'rotate(180deg)'})),
            transition('expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
            ),
        ])
    ]
})
export class MenuItemComponent implements OnInit {
    expanded: boolean;
    @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
    @Input() item: MenuItem;
    @Input() depth: number;

    constructor(
        public router: Router,
        private _menuItemService: MenuItemService
    ) {
        if (this.depth === undefined) {
            this.depth = 0;
        }
    }

    ngOnInit(): void {
        this._menuItemService.currentUrl.subscribe((url: string) => {
            if (this.item.route && url) {
              // console.log(`Checking '/${this.item.route}' against '${url}'`);
              this.expanded = url.indexOf(`/${this.item.route}`) === 0;
              this.ariaExpanded = this.expanded;
              // console.log(`${this.item.route} is expanded: ${this.expanded}`);
            }
        });
    }

    onItemSelected(item: MenuItem) {
        if (!item.children || !item.children.length) {
            this.router.navigate([item.route]);
        //    this._menuItemService.closeNav();
        }

        if (item.children && item.children.length) {
            this.expanded = !this.expanded;
        }
    }
}
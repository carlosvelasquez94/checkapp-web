import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  	selector: 'app-root',
  	templateUrl: './app.component.html',
  	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	  title = 'checkapp-web';
	  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
		iconRegistry.addSvgIcon(
			'hdi_car', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/auto.svg'));

		iconRegistry.addSvgIcon(
			'hdi_embarcacion', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/embarcacion.svg'));

		iconRegistry.addSvgIcon(
			'hdi_inspeccion', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/inspeccion.svg'));

		iconRegistry.addSvgIcon(
			'hdi_danios_1', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/danios-001.svg'));

		iconRegistry.addSvgIcon(
			'hdi_danios_2', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/danios-002.svg'));

		iconRegistry.addSvgIcon(
			'hdi_danios_3', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/danios-003.svg'));

		iconRegistry.addSvgIcon(
			'hdi_cobertura', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/cobertura.svg'));

		iconRegistry.addSvgIcon(
			'hdi_reporte', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/reporte.svg'));
	  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './shared/interceptors/token.interceptor';
import { MatIconModule } from '@angular/material';

@NgModule({
  	declarations: [
    	AppComponent
  	],
  	imports: [
    	BrowserModule,
    	AppRoutingModule,
    	HttpClientModule,
		BrowserAnimationsModule,
		MatIconModule
  	],
  	providers: [
    	{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  	],
  	bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ImagenPipe } from './imagen.pipe';

@NgModule({
    declarations: [ ImagenPipe ],
    exports: [ ImagenPipe ],
    imports: [ CommonModule, RouterModule ]
})

export class ImagenPipeModule { }
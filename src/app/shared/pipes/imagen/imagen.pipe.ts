import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';

@Pipe({
    name: 'imagen'
})

export class ImagenPipe implements PipeTransform {
    transform(img: string, args?: any): any {
        let url = environment.urlService + '/api/imagen' + img;
        return url;
    }
}

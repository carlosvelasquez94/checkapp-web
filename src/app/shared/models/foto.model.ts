export class Foto {
    private tipo: string;
    private imagen: string;

    constructor(obj) {
        this.tipo = obj.tipo || '';
        this.imagen = obj.imagen || '';
    }

    get Tipo(): string {
        return this.tipo;
    }

    get Imagen(): string {
        return this.imagen;
    }
}
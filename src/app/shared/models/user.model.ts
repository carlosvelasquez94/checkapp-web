export class User {
    private id: string;
    private username: string;
    private access_token?: string;
    private refresh_token?: string;

    constructor(id: string, username: string, 
        access_token?: string, refresh_token?: string) {
            this.id = id;
            this.username = username;
            this.access_token = access_token;
            this.refresh_token = refresh_token;
    }

    get Id(): string {
        return this.id;
    }

    set Id(id: string) {
        this.id = id;
    }

    get Username(): string {
        return this.username;
    }

    set Username(username: string) {
        this.username = username;
    }

    get Access_Token(): string {
        return this.access_token;
    }

    set Access_Token(access_token: string) {
        this.access_token = access_token;
    }

    get Refresh_Token(): string {
        return this.refresh_token
    }

    set Refresh_Token(refresh_token: string) {
        this.refresh_token = refresh_token;
    }
}
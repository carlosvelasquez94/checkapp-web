import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError as observableThrowError  } from 'rxjs';
import { 
    HttpInterceptor, 
    HttpRequest, 
    HttpHandler, 
    HttpSentEvent, 
    HttpHeaderResponse, 
    HttpProgressEvent, 
    HttpResponse, 
    HttpUserEvent,
    HttpErrorResponse
} from "@angular/common/http";

import { catchError } from 'rxjs/operators';
import { error } from '@angular/compiler/src/util';
import { AuthService } from '../../login/auth.service';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private _router: Router,
        private _authService: AuthService
    ){ }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        let currentUser = this._authService.currentUserValue;
        if (currentUser && currentUser.Access_Token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${currentUser.Access_Token}`
                }
            });
        }
        return next.handle(request).pipe(
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    switch((<HttpErrorResponse>err).status){
                        case 400:
                            return this.handle400Error(err);
                        case 401:
                            return this.handle400Error(err);
                        case 500:
                            return this.handle500Error(error);
                        default:
                            return this.handle500Error(error);
                    }
                } else {
                    return observableThrowError(err);
                }
            })
        )
    }

    handle400Error(error) {
        this._router.navigate(['/login']);
        return observableThrowError(error);
    }

    handle500Error(error){
        return observableThrowError(error);
    }

    getToken() {
        return this._authService.currentUserValue.Access_Token;
    }

}
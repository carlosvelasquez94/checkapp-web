import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../shared/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
        private _http: HttpClient
    ) { 
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    SignIn(username: string, password: string) {
        let auth = {
            'username': username,
            'password': password,
            'grant_type': 'credentials'
        }
        return this._http.post<any>(`${environment.urlService}/api/auth`, auth).pipe(
            map(result => {
                let user = new User(
                    result.user._id,
                    result.user.username,
                    result.tokenObject.access_token,
                    result.tokenObject.refresh_token
                );
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            })
        )
    }

    SignOut() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { hdiAnimations } from '../animations/index';
import { AuthService } from './auth.service';
import { first } from 'rxjs/operators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: hdiAnimations
})
export class LoginComponent implements OnInit {
    public loading: Boolean = false;
    public loginForm: FormGroup;

    constructor(
        private _formBuilder: FormBuilder,
        private _authService: AuthService,
        private _router: Router
    ) { 

    }

    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            username   : ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    onSubmit(): void {
        this.loading = true;
        this._authService.SignIn(this.loginForm.value.username, this.loginForm.value.password)
        .pipe(first())
        .subscribe(
            data => {
                this._router.navigate(['/pages']);
            },
            error => {
                //this.error = error;
                this.loading = false;
            }
        );
    }
}